"""
Initialize the backend server extension
"""

import notebook.base.handlers
import notebook.utils


class HelloHandler(notebook.base.handlers.IPythonHandler):
    def get(self):
        """Handler for /hello"""
        self.finish('{"text":"Hello, world!"}')


def load_jupyter_server_extension(nb_server):
    """Backend Jupyter server extension."""
    web_app = nb_server.web_app
    srv_handlers = [
        ("/hello", HelloHandler),
    ]
    base_url = web_app.settings["base_url"]
    handlers = [(notebook.utils.url_path_join(base_url, x[0]), x[1]) for x in srv_handlers]
    web_app.add_handlers(".*", handlers)
