# jlab_simple_git_submit

A JupyterLab extension to bundleup changes in a local git workspace to be submitted to a branch upstream.

To run:
PYTHONPATH=.:${PYTHONPATH} jupyter lab --NotebookApp.nbserver_extensions="{'jlab_simple_git_submit':True}" --watch
