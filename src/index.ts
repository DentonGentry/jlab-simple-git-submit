import {
  JupyterLab, JupyterLabPlugin
} from '@jupyterlab/application';

import {
  ICommandPalette
} from '@jupyterlab/apputils';

import {
  Widget
} from '@phosphor/widgets';

import '../style/index.css';


/**
 * Initialization data for the jlab_simple_git_submit extension.
 */
const extension: JupyterLabPlugin<void> = {
  id: 'jlab_simple_git_submit',
  autoStart: true,
  requires: [ICommandPalette],
  activate: (app: JupyterLab, palette: ICommandPalette) => {
    console.log('JupyterLab extension jlab_simple_git_submit is activated!');
    const command: string = 'hello:hello';

    // Create a single widget
    let widget: Widget = new Widget();
    widget.id = 'hello-text';
    widget.title.label = 'Hello';
    widget.title.closable = true;

    app.commands.addCommand(command, {
      label: 'Hello',
      execute: () => {
        if (!widget.isAttached) {
          // Attach the widget to the main work area if it's not there
          app.shell.addToMainArea(widget);
        }
        // Activate the widget
        app.shell.activateById(widget.id);
      }
    });

    let div = document.createElement('div');
    widget.node.appendChild(div);
    fetch('/hello').then(response => {
      return response.json();
    }).then(data => {
      div.innerHTML = data.text;
    });

    // Add the command to the palette.
    palette.addItem({command, category: 'Tutorial'});
  }
};


export default extension;
